let pgp_word_list = [|
  '\x00', "aardvark", "adroitness";
  '\x01', "absurd", "adviser";
  '\x02', "accrue", "aftermath";
  '\x03', "acme", "aggregate";
  '\x04', "adrift", "alkali";
  '\x05', "adult", "almighty";
  '\x06', "afflict", "amulet";
  '\x07', "ahead", "amusement";
  '\x08', "aimless", "antenna";
  '\x09', "Algol", "applicant";
  '\x0A', "allow", "Apollo";
  '\x0B', "alone", "armistice";
  '\x0C', "ammo", "article";
  '\x0D', "ancient", "asteroid";
  '\x0E', "apple", "Atlantic";
  '\x0F', "artist", "atmosphere";
  '\x10', "assume", "autopsy";
  '\x11', "Athens", "Babylon";
  '\x12', "atlas", "backwater";
  '\x13', "Aztec", "barbecue";
  '\x14', "baboon", "belowground";
  '\x15', "backfield", "bifocals";
  '\x16', "backward", "bodyguard";
  '\x17', "banjo", "bookseller";
  '\x18', "beaming", "borderline";
  '\x19', "bedlamp", "bottomless";
  '\x1A', "beehive", "Bradbury";
  '\x1B', "beeswax", "bravado";
  '\x1C', "befriend", "Brazilian";
  '\x1D', "Belfast", "breakaway";
  '\x1E', "berserk", "Burlington";
  '\x1F', "billiard", "businessman";
  '\x20', "bison", "butterfat";
  '\x21', "blackjack", "Camelot";
  '\x22', "blockade", "candidate";
  '\x23', "blowtorch", "cannonball";
  '\x24', "bluebird", "Capricorn";
  '\x25', "bombast", "caravan";
  '\x26', "bookshelf", "caretaker";
  '\x27', "brackish", "celebrate";
  '\x28', "breadline", "cellulose";
  '\x29', "breakup", "certify";
  '\x2A', "brickyard", "chambermaid";
  '\x2B', "briefcase", "Cherokee";
  '\x2C', "Burbank", "Chicago";
  '\x2D', "button", "clergyman";
  '\x2E', "buzzard", "coherence";
  '\x2F', "cement", "combustion";
  '\x30', "chairlift", "commando";
  '\x31', "chatter", "company";
  '\x32', "checkup", "component";
  '\x33', "chisel", "concurrent";
  '\x34', "choking", "confidence";
  '\x35', "chopper", "conformist";
  '\x36', "Christmas", "congregate";
  '\x37', "clamshell", "consensus";
  '\x38', "classic", "consulting";
  '\x39', "classroom", "corporate";
  '\x3A', "cleanup", "corrosion";
  '\x3B', "clockwork", "councilman";
  '\x3C', "cobra", "crossover";
  '\x3D', "commence", "crucifix";
  '\x3E', "concert", "cumbersome";
  '\x3F', "cowbell", "customer";
  '\x40', "crackdown", "Dakota";
  '\x41', "cranky", "decadence";
  '\x42', "crowfoot", "December";
  '\x43', "crucial", "decimal";
  '\x44', "crumpled", "designing";
  '\x45', "crusade", "detector";
  '\x46', "cubic", "detergent";
  '\x47', "dashboard", "determine";
  '\x48', "deadbolt", "dictator";
  '\x49', "deckhand", "dinosaur";
  '\x4A', "dogsled", "direction";
  '\x4B', "dragnet", "disable";
  '\x4C', "drainage", "disbelief";
  '\x4D', "dreadful", "disruptive";
  '\x4E', "drifter", "distortion";
  '\x4F', "dropper", "document";
  '\x50', "drumbeat", "embezzle";
  '\x51', "drunken", "enchanting";
  '\x52', "Dupont", "enrollment";
  '\x53', "dwelling", "enterprise";
  '\x54', "eating", "equation";
  '\x55', "edict", "equipment";
  '\x56', "egghead", "escapade";
  '\x57', "eightball", "Eskimo";
  '\x58', "endorse", "everyday";
  '\x59', "endow", "examine";
  '\x5A', "enlist", "existence";
  '\x5B', "erase", "exodus";
  '\x5C', "escape", "fascinate";
  '\x5D', "exceed", "filament";
  '\x5E', "eyeglass", "finicky";
  '\x5F', "eyetooth", "forever";
  '\x60', "facial", "fortitude";
  '\x61', "fallout", "frequency";
  '\x62', "flagpole", "gadgetry";
  '\x63', "flatfoot", "Galveston";
  '\x64', "flytrap", "getaway";
  '\x65', "fracture", "glossary";
  '\x66', "framework", "gossamer";
  '\x67', "freedom", "graduate";
  '\x68', "frighten", "gravity";
  '\x69', "gazelle", "guitarist";
  '\x6A', "Geiger", "hamburger";
  '\x6B', "glitter", "Hamilton";
  '\x6C', "glucose", "handiwork";
  '\x6D', "goggles", "hazardous";
  '\x6E', "goldfish", "headwaters";
  '\x6F', "gremlin", "hemisphere";
  '\x70', "guidance", "hesitate";
  '\x71', "hamlet", "hideaway";
  '\x72', "highchair", "holiness";
  '\x73', "hockey", "hurricane";
  '\x74', "indoors", "hydraulic";
  '\x75', "indulge", "impartial";
  '\x76', "inverse", "impetus";
  '\x77', "involve", "inception";
  '\x78', "island", "indigo";
  '\x79', "jawbone", "inertia";
  '\x7A', "keyboard", "infancy";
  '\x7B', "kickoff", "inferno";
  '\x7C', "kiwi", "informant";
  '\x7D', "klaxon", "insincere";
  '\x7E', "locale", "insurgent";
  '\x7F', "lockup", "integrate";
  '\x80', "merit", "intention";
  '\x81', "minnow", "inventive";
  '\x82', "miser", "Istanbul";
  '\x83', "Mohawk", "Jamaica";
  '\x84', "mural", "Jupiter";
  '\x85', "music", "leprosy";
  '\x86', "necklace", "letterhead";
  '\x87', "Neptune", "liberty";
  '\x88', "newborn", "maritime";
  '\x89', "nightbird", "matchmaker";
  '\x8A', "Oakland", "maverick";
  '\x8B', "obtuse", "Medusa";
  '\x8C', "offload", "megaton";
  '\x8D', "optic", "microscope";
  '\x8E', "orca", "microwave";
  '\x8F', "payday", "midsummer";
  '\x90', "peachy", "millionaire";
  '\x91', "pheasant", "miracle";
  '\x92', "physique", "misnomer";
  '\x93', "playhouse", "molasses";
  '\x94', "Pluto", "molecule";
  '\x95', "preclude", "Montana";
  '\x96', "prefer", "monument";
  '\x97', "preshrunk", "mosquito";
  '\x98', "printer", "narrative";
  '\x99', "prowler", "nebula";
  '\x9A', "pupil", "newsletter";
  '\x9B', "puppy", "Norwegian";
  '\x9C', "python", "October";
  '\x9D', "quadrant", "Ohio";
  '\x9E', "quiver", "onlooker";
  '\x9F', "quota", "opulent";
  '\xA0', "ragtime", "Orlando";
  '\xA1', "ratchet", "outfielder";
  '\xA2', "rebirth", "Pacific";
  '\xA3', "reform", "pandemic";
  '\xA4', "regain", "Pandora";
  '\xA5', "reindeer", "paperweight";
  '\xA6', "rematch", "paragon";
  '\xA7', "repay", "paragraph";
  '\xA8', "retouch", "paramount";
  '\xA9', "revenge", "passenger";
  '\xAA', "reward", "pedigree";
  '\xAB', "rhythm", "Pegasus";
  '\xAC', "ribcage", "penetrate";
  '\xAD', "ringbolt", "perceptive";
  '\xAE', "robust", "performance";
  '\xAF', "rocker", "pharmacy";
  '\xB0', "ruffled", "phonetic";
  '\xB1', "sailboat", "photograph";
  '\xB2', "sawdust", "pioneer";
  '\xB3', "scallion", "pocketful";
  '\xB4', "scenic", "politeness";
  '\xB5', "scorecard", "positive";
  '\xB6', "Scotland", "potato";
  '\xB7', "seabird", "processor";
  '\xB8', "select", "provincial";
  '\xB9', "sentence", "proximate";
  '\xBA', "shadow", "puberty";
  '\xBB', "shamrock", "publisher";
  '\xBC', "showgirl", "pyramid";
  '\xBD', "skullcap", "quantity";
  '\xBE', "skydive", "racketeer";
  '\xBF', "slingshot", "rebellion";
  '\xC0', "slowdown", "recipe";
  '\xC1', "snapline", "recover";
  '\xC2', "snapshot", "repellent";
  '\xC3', "snowcap", "replica";
  '\xC4', "snowslide", "reproduce";
  '\xC5', "solo", "resistor";
  '\xC6', "southward", "responsive";
  '\xC7', "soybean", "retraction";
  '\xC8', "spaniel", "retrieval";
  '\xC9', "spearhead", "retrospect";
  '\xCA', "spellbind", "revenue";
  '\xCB', "spheroid", "revival";
  '\xCC', "spigot", "revolver";
  '\xCD', "spindle", "sandalwood";
  '\xCE', "spyglass", "sardonic";
  '\xCF', "stagehand", "Saturday";
  '\xD0', "stagnate", "savagery";
  '\xD1', "stairway", "scavenger";
  '\xD2', "standard", "sensation";
  '\xD3', "stapler", "sociable";
  '\xD4', "steamship", "souvenir";
  '\xD5', "sterling", "specialist";
  '\xD6', "stockman", "speculate";
  '\xD7', "stopwatch", "stethoscope";
  '\xD8', "stormy", "stupendous";
  '\xD9', "sugar", "supportive";
  '\xDA', "surmount", "surrender";
  '\xDB', "suspense", "suspicious";
  '\xDC', "sweatband", "sympathy";
  '\xDD', "swelter", "tambourine";
  '\xDE', "tactics", "telephone";
  '\xDF', "talon", "therapist";
  '\xE0', "tapeworm", "tobacco";
  '\xE1', "tempest", "tolerance";
  '\xE2', "tiger", "tomorrow";
  '\xE3', "tissue", "torpedo";
  '\xE4', "tonic", "tradition";
  '\xE5', "topmost", "travesty";
  '\xE6', "tracker", "trombonist";
  '\xE7', "transit", "truncated";
  '\xE8', "trauma", "typewriter";
  '\xE9', "treadmill", "ultimate";
  '\xEA', "Trojan", "undaunted";
  '\xEB', "trouble", "underfoot";
  '\xEC', "tumor", "unicorn";
  '\xED', "tunnel", "unify";
  '\xEE', "tycoon", "universe";
  '\xEF', "uncut", "unravel";
  '\xF0', "unearth", "upcoming";
  '\xF1', "unwind", "vacancy";
  '\xF2', "uproot", "vagabond";
  '\xF3', "upset", "vertigo";
  '\xF4', "upshot", "Virginia";
  '\xF5', "vapor", "visitor";
  '\xF6', "village", "vocalist";
  '\xF7', "virus", "voyager";
  '\xF8', "Vulcan", "warranty";
  '\xF9', "waffle", "Waterloo";
  '\xFA', "wallet", "whimsical";
  '\xFB', "watchword", "Wichita";
  '\xFC', "wayside", "Wilmington";
  '\xFD', "willow", "Wyoming";
  '\xFE', "woodlark", "yesteryear";
  '\xFF', "Zulu", "Yucatan";
|]

let encode str =
  let result = Array.make (String.length str) "" in
  StringLabels.iteri str ~f:begin fun index char ->
    let (_, even, odd) = Array.get pgp_word_list (int_of_char char) in
    result.(index) <- if index mod 2 = 0 then even else odd
  end;
  result

type recoverable_error =
  [ `Alternance of int | `Case of int ]
type error =
  [ recoverable_error | `Unknown_word of int * string ]
type decode_result = string * [
    | `Complete of recoverable_error list
    | `Incomplete of error list
  ]
let decode ?buffer word_array : decode_result =
  let buffer =
    match buffer with
    | Some b -> b
    | None -> Bytes.create (Array.length word_array) in
  let recoverable_errors = ref [] in
  let module M = struct
    exception Found of bool * bool * char (* even/odd × case × char *)
    exception Unknown_word of int * string
  end in
  begin try
    ArrayLabels.iteri word_array ~f:begin fun index word ->
      let char =
        let cmp a b =
          String.lowercase_ascii a = String.lowercase_ascii b in
        try
          ArrayLabels.iter pgp_word_list ~f:begin fun (char, even, odd) ->
            if cmp word even then 
              raise (M.Found (index mod 2 = 0, word = even, char))
            else if cmp word odd then 
              raise (M.Found (index mod 2 = 1, word = odd, char))
            else
              ()
          end;
          raise (M.Unknown_word (index, word))
        with M.Found (oddness, case, char) ->
          if not oddness then
            recoverable_errors := `Alternance index :: !recoverable_errors;
          if not case then
            recoverable_errors := `Case index :: !recoverable_errors;
          char
      in
      Bytes.set buffer index char
    end;
    (Bytes.sub_string buffer 0 (Array.length word_array),
     `Complete !recoverable_errors)
  with
  | M.Unknown_word (index, word) ->
    (Bytes.sub_string buffer 0 index,
     `Incomplete (`Unknown_word (index, word) ::
                  (!recoverable_errors :> error list)))
  end

let error_to_string =
  let open Printf in
  function
  | `Alternance i -> sprintf "even/odd alternance error at word %d" i
  | `Case i -> sprintf "Case mismatch at word %d" i
  | `Unknown_word (i, wd) -> sprintf "Unknown word %S (index %d)" wd i 

let decode_result_to_string (s, status) =
  let open Printf in
  match status with
  | `Complete [] ->
    sprintf "Complete: %S (no errors)" s
  | `Complete e ->
    sprintf "Complete: %S (recoverable errors: [%s])" s
      (List.rev_map error_to_string e |> String.concat ", ")
  | `Incomplete e ->
    sprintf "Incomplete: %S (errors: [%s])" s
      (List.rev_map error_to_string e |> String.concat ", ")