(** Quick conversion between words from the
    {{:https://en.wikipedia.org/wiki/PGP_word_list}PGP Word List}
    and byte arrays.
*)

val pgp_word_list : (char * string * string) array
(** 
   The word-list itself as an array “character × even-word × odd-word.”
 *)

val encode : string -> string array
(** Encore an arbitrary sequence of bytes string into a list of words. *)

type recoverable_error = [ `Alternance of int | `Case of int ]
(** Non-fatal decoding errors. *)

type error =
  [ recoverable_error | `Unknown_word of int * string ]
(** All possible decoding errors. *)

type decode_result = string * [
  | `Complete of recoverable_error list
  | `Incomplete of error list
]
(** The result of decoding a sequence of words. If only recoverable
    errors happen the result is [`Complete _], if one fatal error happens
    (i.e. [`Unknown_word _]) then it is [`Incomplete _]. *)

val decode : ?buffer:bytes -> string array -> decode_result
(** Decode a sequence of words, if [?buffer] is [None] the function
    create a string of the same length as the input array, if the user
    provides the reusable [~buffer] it is up to them to ensure that the
    buffer is long enough. *)

val error_to_string : error -> string
(** Create a display-friendly string for an error. *)

val decode_result_to_string : decode_result -> string
(** Create a display-friendly string for a decoding result. *)