Pewolio: PGP Word List In OCaml
===============================

A PGP Word List Library for OCaml

Simple and stupid conversion between byte sequences and pronounceable
words, see wikipedia:
[PGP word list](https://en.wikipedia.org/wiki/PGP_word_list).


Build
-----

    jbuilder build
    
Test
----

Just build and run them (requires `alcotest`):

    jbuilder build test/main.exe
    _build/default/test/main.exe

Making Of
---------

See file:

[`kronolynx/pgp-word-list-converter/blob/master/pgpWordList.js`](https://github.com/kronolynx/pgp-word-list-converter/blob/master/pgpWordList.js)

copy-paste into Emacs-evil or Vim buffer and:

```
%s/^ *'\([A-F0-9]*\)': *\['\([a-zA-Z]*\)', *'\([a-zA-Z]*\)'.*$/'\\x\1', "\2", "\3";/
```

