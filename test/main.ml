module List = ListLabels
open Printf

let decode_result =
  Alcotest.testable Fmt.(of_to_string Pewolio.decode_result_to_string)
    begin fun (a_string, a_status) (b_string, b_status) ->
      let same_errors la lb =
        List.sort_uniq ~cmp:compare la = List.sort_uniq ~cmp:compare lb in
      match a_status, b_status with
      | `Complete aerrors, `Complete berrors ->
        a_string = b_string && same_errors aerrors berrors
      | `Incomplete aerrors, `Incomplete berrors ->
        a_string = b_string && same_errors aerrors berrors
      | _, _ -> false
    end

let enc_dec ?buffer () = [
  "";
  "a";
  "ab\000ababab";
  "\000";
  "ab\000aba\n\t baœ ö 🎶 b";
  String.make 100_000 'B';
] |> List.map ~f:(fun v ->
    let name =
      sprintf "enc-dec-%S" (String.sub v 0 (min (String.length v) 60)) in
    name, `Quick, (fun () ->
        let res = v, `Complete [] in
        Alcotest.(check decode_result)
          name res Pewolio.(encode v |> decode ?buffer))
    )

let more_decs ?buffer () = [
  ["noword"]                        , ("", `Incomplete [`Unknown_word (0, "noword")]);
  ["aardvark"]                      , ("\000", `Complete   []);
  ["aardvark"; "nope"]              , ("\000", `Incomplete [`Unknown_word (1, "nope")]);
  ["aardvark"; "adviser"]           , ("\000\001", `Complete   []);
  ["aardvark"; "absurd"]            , ("\000\001", `Complete   [`Alternance 1]);
  ["aardvark"; "absurd"; "Jupiter"] , ("\x00\x01\x84" , `Complete [`Alternance 1; `Alternance 2]);
  ["aardvark"; "absurd"; "jupiter"],  ("\x00\x01\x84", `Complete [`Alternance 1; `Alternance 2; `Case 2]);
]
|> List.map ~f:begin fun (wdlist, res) ->
      let name = sprintf "dec-%s" (String.concat "-" wdlist) in
      name, `Quick, (fun () ->
          let words = Array.of_list wdlist in
          Alcotest.(check decode_result)
            name res Pewolio.(decode ?buffer words))
end

let () =
  let buffer = Bytes.(create 100_001) in
  Alcotest.run "My first test" [
    "enc-dec", enc_dec ();
    "enc-dec-with-buf", enc_dec ~buffer ();
    "more-decs", more_decs ();
    "more-decs-with-buffer", more_decs ~buffer ();
  ]